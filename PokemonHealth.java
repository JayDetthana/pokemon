import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class PokemonHealth extends JFrame{
    private ArrayList<Pokemon> Bag;
    private Trainer trainer;
    public PokemonHealth(Trainer trainer, Pokemon myPokemon){
        super("Catch Game");
        this.trainer = trainer;

        Container c = getContentPane();
        ImageIcon img = new ImageIcon("pic\\potioncomplete.gif");
        JLabel background = new JLabel(img);
        c.add(background);
        pack();

        do{
            System.out.println("Get HP my Pet : " + myPokemon.getHP());
            if(myPokemon.getHP() >= myPokemon.getMaxHP()){
                System.out.println("Break");
                break;
            }else{
                myPokemon.Health(trainer);
            }
        }while(true);

        setSize(500, 500);
        setVisible(true);
    }
}
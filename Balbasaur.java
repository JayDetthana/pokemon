
public class Balbasaur extends Pokemon{
    public Balbasaur(String name){
        super(name, "Balbasaur", (int)(Math.random() * 150), 25, 10, "pic\\balbasaur01.gif", "pic\\balbasaur02.gif");
    }
    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + getName() + " attack " + enemy.getName());
        int value = 25 - (int)(enemy.getDEF());
        if(value <= 0){
            enemy.Damage(1);
        }else{
            enemy.Damage(value);
        }
        // enemy.Damage(10);
    }
}

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PokemonInventory extends JFrame{
    private PotionBag myPotionBag;
    private Trainer trainer;
    public PokemonInventory(Trainer trainer){
        super("Inventory");
        this.myPotionBag = trainer.getPotionbag();
        this.trainer = trainer;
        Container c = getContentPane();

        JLabel name = new JLabel(myPotionBag.getName());
        name.setFont(new Font("Courier New", Font.ITALIC, 40));
        JLabel EA = new JLabel("EA : " + myPotionBag.getEA());
        EA.setFont(new Font("Courier New", Font.ITALIC, 40));

        ImageIcon img = new ImageIcon("pic\\scroll01.png");
        JLabel background = new JLabel(img);

        ImageIcon icon_potion = new ImageIcon(myPotionBag.getURL_Potion());
        JLabel iconpotion = new JLabel(icon_potion);

        JButton shop = new JButton();
        shop.setIcon(new ImageIcon("pic\\shop_button.png"));

        shop.setBounds(90, 420, 200, 50);
        name.setBounds(100, 220, 1000, 100);
        EA.setBounds(100, 260, 1000, 100);
        iconpotion.setBounds(120, 100, 150, 150);
        
        c.add(background);
        background.add(iconpotion);
        background.add(name);
        background.add(EA);
        background.add(shop);
        pack();
        
        shop.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ShopStore ps = new ShopStore(trainer);
                setVisible(false);
            }
        });

        setSize(430, 820);
        setVisible(true);
    }
}
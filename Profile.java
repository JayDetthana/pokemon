import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.Timer;

public class Profile extends JFrame{
    private Trainer trainer;
    public Profile(Trainer trainer){
        super("Profile");
        this.trainer = trainer;

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        System.out.println("--------------------------------");
        System.out.println("Profile");

        Container c = getContentPane();

        ImageIcon img = new ImageIcon("pic\\namecard.gif");
        JLabel background = new JLabel(img);
        JLabel name = new JLabel(trainer.getName());
        name.setFont(new Font("Courier New", Font.ITALIC, 40));
        JLabel money = new JLabel("Money : $" + trainer.getMoney());
        money.setFont(new Font("Courier New", Font.ITALIC, 40));


        JButton edit = new JButton();
        edit.setIcon(new ImageIcon("pic\\edit_button.png"));
        JButton menu = new JButton();
        menu.setIcon(new ImageIcon("pic\\menu_button.png"));

        c.setLayout(new FlowLayout());
        background.setLayout(null);
        name.setBounds(600, 315, 500, 50);
        money.setBounds(600, 455, 500, 50);
        menu.setBounds(270, 200, 100, 50);
        edit.setBounds(835, 400, 100, 50);
        
        c.add(background);
        background.add(name);
        background.add(money);
        background.add(menu);
        background.add(edit);

        edit.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                String name = JOptionPane.showInputDialog(null, "Enter Your Name: ", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(name != null){
                        setVisible(false);
                        System.out.println("Enter name : " + name);
                        Trainer t = new Trainer(name);
                        MainGame mg = new MainGame(t);
                    }
            }
        });
        menu.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                MainGame mg = new MainGame(trainer);
            }
        });

        System.out.println("--------------------------------");

        setSize(1220, 740);
        setVisible(true);
    }

}
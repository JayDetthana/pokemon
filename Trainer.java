import java.util.*;

public class Trainer {
    private ArrayList<Pokemon> Bag;
    private Scanner sc;
    private String name;
    private int money = 0;
    public PotionBag potion = new PotionBag("Potion", 30, 30, "pic\\potionicon.png");
    public Trainer(String name){
        Bag = new ArrayList<Pokemon>();
        Pokemon Pikachu = new Pikachu("Pikachu");
        Pokemon Balbasaur = new Balbasaur("Balbasaur");
        Pokemon Tyranitar = new Tyranitar("Tyranitar");
        Pokemon Kabigon = new Kabigon("Kabigon");

        Bag.add(Pikachu);
        Bag.add(Kabigon);
        Bag.add(Tyranitar);
        Bag.add(Balbasaur);

        sc = new Scanner(System.in);
        
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public void setMoney(int money){
        this.money = money;
    }
    public int getMoney(){
        return money;
    }
    public void play(){
        String cmd = "";
        do{
            System.out.println("ENTER COMAND : ");
            System.out.println(" print -> show Pokemon");
            System.out.println(" q -> to Exit");
            cmd = sc.nextLine();
            if(cmd.equals("print")){
                printPokemon(Bag);
            }
            if(cmd.equals("catch")){
                CatchPokemon();
            }
        }while(!cmd.equals("q"));
    }

    public void CatchPokemon(){
        System.out.println("Pokemon in Bag");
        ArrayList<Pokemon> Pokemons = PokemonRandomrizer.getPokemons(5);
        
        System.out.println("Pokemon around you");
        int no_wildPokemon = 0;
        int no_myPokemon = 0;
        printPokemon(Pokemons);

        System.out.println("Select :");
        no_wildPokemon = sc.nextInt();
        if(no_wildPokemon < 0){
            return;
        }
        Pokemon wildPokemon = Pokemons.get(no_wildPokemon);

        System.out.println("Pokemon in bag :");
        printPokemon(Bag);
        System.out.println("Select Pokemon in bag :");
        
        no_myPokemon = sc.nextInt();
        Pokemon myPokemon = Bag.get(no_myPokemon);

        boolean isWin = false;
        do{
            myPokemon.attack(wildPokemon);
            if(wildPokemon.getHP() == 0){
                isWin = true;
                break;
            }else{
                wildPokemon.attack(myPokemon);
                if(myPokemon.getHP() == 0){
                    isWin = false;
                    break;
                }
            }
        }while(true);

        if(isWin){
            System.out.println("You win ");
            Bag.add(wildPokemon);
        }else{
            System.out.println(wildPokemon.getName() + " win ");
        }

        System.out.println("You catch :");
        sc.nextLine();
    }

    public void printPokemon(ArrayList<Pokemon> Pokemons){
        System.out.println("Pokemon in Bag");
        int number = 0;
        for(Pokemon p: Pokemons){
            System.out.println(" " + number + " " + p + " " + " HP " + " " + p.getHP());
            number++;
        }
    }
    public ArrayList<Pokemon> getBag(){
        return Bag;
    }
    public PotionBag getPotionbag(){
        return potion;
    }
}
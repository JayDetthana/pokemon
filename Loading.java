
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.Timer;

public class Loading extends JFrame {
    public Loading(){
        super("Loading Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);

        System.out.println("--------------------------------");
        System.out.println("Loading");

        Container c = getContentPane();
        ImageIcon img = new ImageIcon("pic\\loading.gif");
        JLabel background = new JLabel(img);
        JButton enter = new JButton();
        enter.setIcon(new ImageIcon("pic\\button-enter.gif"));

        c.setLayout(new FlowLayout());
        background.setLayout(null);
        c.add(background);
        pack();

        enter.setBounds(310, 420, 181, 50);

        background.add(enter);

        enter.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                String name = JOptionPane.showInputDialog(null, "Enter Your Name: ", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(name != null){
                        setVisible(false);
                        System.out.println("Enter name : " + name);
                        Trainer t = new Trainer(name);
                        MainGame mg = new MainGame(t);
                    }
            }
        });
        setSize(800,650);
    }
}
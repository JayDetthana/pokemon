import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class PokemonStatus extends JFrame{
    Pokemon pokemon;
    public PokemonStatus(Trainer trainer, Pokemon pokemon, int enableAttack){
        super("Pokemon Status: " + pokemon.getName());
        this.pokemon = pokemon;

        setResizable(false);
        setVisible(true);

        // debug Start
        System.out.println("--------------------------------");
        System.out.println("Status : " + pokemon.getName());
        System.out.println("> HP : " + pokemon.getHP() + "||" + "MAXHP : " + pokemon.getMaxHP());
        System.out.println("> ATK : " + pokemon.getATK() + "||" + "DEF : " + pokemon.getDEF());
        System.out.println("> URL Pic: " + pokemon.getURL_pic());
        // debug End

        Container c = getContentPane();

        JLabel name = new JLabel(pokemon.getName());
        name.setFont(new Font("Courier New", Font.ITALIC, 25));
        JLabel HP = new JLabel("HP : " + pokemon.getHP());
        HP.setFont(new Font("Courier New", Font.ITALIC, 40));
        JLabel ATK = new JLabel("ATK : " + pokemon.getATK());
        ATK.setFont(new Font("Courier New", Font.ITALIC, 40));
        JLabel DEF = new JLabel("DEF : " + pokemon.getDEF());
        DEF.setFont(new Font("Courier New", Font.ITALIC, 40));

        JButton usePotion = new JButton();
        usePotion.setIcon(new ImageIcon("pic\\use_potion.png"));
        JButton menu = new JButton();
        menu.setIcon(new ImageIcon("pic\\menu_button.png"));
        JButton attack = new JButton();
        attack.setIcon(new ImageIcon("pic\\attackbutton.png"));

        ImageIcon img = new ImageIcon(pokemon.getURL_pic());
        JLabel background = new JLabel(img);
        c.setLayout(new FlowLayout());
        background.setLayout(null);

        name.setBounds(100, 250, 1000, 100);
        HP.setBounds(100, 300, 1000, 100);
        ATK.setBounds(100, 350, 1000, 100);
        DEF.setBounds(100, 400, 1000, 100);
        usePotion.setBounds(600, 520, 300, 150);
        menu.setBounds(10, 10, 100, 50);
        attack.setBounds(600, 600, 400, 100);
        
        c.add(background);
        background.add(name);
        background.add(HP);
        background.add(ATK);
        background.add(DEF);
        background.add(menu);
        if(enableAttack == 1){
            background.add(attack);
        }
        pack();

        if(pokemon.getHP() != pokemon.getMaxHP()){
            background.add(usePotion);
        }

        attack.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                AttackinCatch aa = new AttackinCatch(trainer, pokemon);
                setVisible(false);
            }
        });
        menu.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });
        usePotion.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                PokemonHealth ph = new PokemonHealth(trainer, pokemon);
                setVisible(false);
            }
        });
            setSize(1440, 850);
    }
}
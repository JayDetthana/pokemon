
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainGame extends JFrame{
    private Trainer trainer;

    public MainGame(Trainer trainer){
        super("Pokemon  Game");
        this.trainer = trainer;

        System.out.println("--------------------------------");
        System.out.println("Main Menu");

        Container c = getContentPane();
        JLabel trainerNameLabel = new JLabel(this.trainer.getName());
        trainerNameLabel.setFont(new Font("Courier New", Font.ITALIC, 40));
        
        JButton Profile = new JButton();
        Profile.setIcon(new ImageIcon("pic\\profile_button.png"));
        JButton Catch = new JButton();
        Catch.setIcon(new ImageIcon("pic\\catch_button.png"));
        JButton shop = new JButton();
        shop.setIcon(new ImageIcon("pic\\shop_button.png"));
        JButton Status = new JButton();
        Status.setIcon(new ImageIcon("pic\\status_button.png"));
        JButton Inventory = new JButton();
        Inventory.setIcon(new ImageIcon("pic\\inventory_button.png"));


        ImageIcon img = new ImageIcon("pic\\bg02.gif");
        JLabel background = new JLabel(img);
        
        trainerNameLabel.setBounds(480, 200, 500, 100);
        Profile.setBounds(520, 320, 200, 50);
        Catch.setBounds(520, 390, 200, 50);
        shop.setBounds(520, 460, 200, 50);
        Status.setBounds(520, 530, 200, 50);
        Inventory.setBounds(520, 600, 200, 50);
        background.setBounds(0, 0, 1200, 700);
        
        c.add(background);
        c.setLayout(null);
        background.add(trainerNameLabel);
        background.add(Profile);
        background.add(Catch);
        background.add(shop);
        background.add(Status);
        background.add(Inventory);
        pack();

        Profile.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                Profile p = new Profile(trainer);
            }
        });
        Catch.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                CatchGame c = new CatchGame(trainer);
            }
        });
        shop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                ShopStore ps = new ShopStore(trainer);
            }
        });
        Status.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Pokemonbag pb = new Pokemonbag(trainer);
            }
        });
        Inventory.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                PokemonInventory pi = new PokemonInventory(trainer);
            }
        });

        System.out.println("--------------------------------");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1220, 740);
        setVisible(true);
    }
}
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class PopupCatch extends JFrame{
    private Trainer trainer;
    private ArrayList<Pokemon> Pokemons;
    public PopupCatch(Trainer trainer){
        super("Catch Game");
        this.trainer = trainer;
        setResizable(false);

        System.out.println("--------------------------------");
        System.out.println("Catch Game");
        Pokemons = PokemonRandomrizer.getPokemons(4);
        trainer.printPokemon(Pokemons);

        JFrame f = new JFrame("GridLayout");
        JPanel MyPanel = new JPanel();
        JButton ibutton[] = {new JButton(), new JButton(), new JButton(), new JButton()};
        
        MyPanel.setLayout( new GridLayout(2, 2) );  // 4x3 Grid
        
        int i = 0;
        for(Pokemon p: Pokemons){
            ibutton[i].setIcon(new ImageIcon(p.getURL_logo()));
            MyPanel.add(ibutton[i]);
            System.out.println(i + "> Name Pokemon ADD : " + p.getName());
            ibutton[i].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    PokemonStatus pi = new PokemonStatus(trainer, p, 1);
                }
            });
            i++;
        }
        f.getContentPane().add(MyPanel, "Center");
        pack();

        f.setSize(1000, 1000);
   	    f.setVisible(true);
        
        System.out.println("--------------------------------");

    }
}
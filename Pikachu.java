
public class Pikachu extends Pokemon{
    public Pikachu(String name){
        super(name, "Pikachu", 90, 21, 9, "pic\\pikachu03.gif", "pic\\pikachu04.gif");
    }

    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + getName() + "attack " + enemy.getName());
        int value = 21 - (int)(enemy.getDEF());
        System.out.println("ATK : " + value);
        if(value <= 0){
            enemy.Damage(1);
        }else{
            enemy.Damage(value);
        }
        // enemy.Damage(11);
    }
}
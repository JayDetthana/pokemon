
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class CatchGame extends JFrame{
    private Trainer trainer;
    public CatchGame(Trainer trainer){
        super("Catch Game");
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);

        System.out.println("--------------------------------");
        System.out.println("Catch Game");

        Container c = getContentPane();

        ImageIcon img = new ImageIcon("pic\\theme_catch.gif");
        JLabel background = new JLabel(img);

        JButton menu = new JButton();
        menu.setIcon(new ImageIcon("pic\\menu_button.png"));
        JButton find = new JButton();
        find.setIcon(new ImageIcon("pic\\findbutton.gif"));

        find.setBounds(650, 250, 400, 250);
        menu.setBounds(10, 10, 100, 50);

        c.add(background);
        background.add(find);
        background.add(menu);
        pack();

        menu.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                MainGame mg = new MainGame(trainer);
            }
        });
        find.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                // PopupCatch pc = new PopupCatch(trainer);
                PopupCatch pc = new PopupCatch(trainer);
            }
        });


        System.out.println("--------------------------------");
        setSize(1220, 740);
    }
}

public class Raichu extends Pokemon {
    public Raichu(String name){
        super(name, "Raichu", (int)(Math.random() * 500), 51, 19, "pic\\raichu03.gif", "pic\\raichu02.gif");
    }
    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + getName() + "attack " + enemy.getName());
        int value = 51 - (int)(enemy.getDEF());
        if(value <= 0){
            enemy.Damage(1);
        }else{
            enemy.Damage(value);
        }
    }
}

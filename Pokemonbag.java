import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Pokemonbag extends JFrame{
    private Trainer trainer;
    public Pokemonbag(Trainer trainer){
        super("Pokemon Bag");
        this.trainer = trainer;

        Container c = getContentPane();
        ImageIcon img = new ImageIcon("pic\\scroll01.png");
        JLabel background = new JLabel(img);
        JButton ibutton[] = {new JButton(),
                            new JButton(), 
                            new JButton(), 
                            new JButton(), 
                            new JButton(), 
                            new JButton(), 
                            new JButton(), 
                            new JButton(), 
                            new JButton()};
        
        c.add(background);

        int i = 0;
        int height = 100;
        System.out.println("--------------------------------");
        System.out.println("Pokemon Bag");
        for(Pokemon p: trainer.getBag()){
            System.out.println("> " + p.getName());
            ibutton[i] = new JButton(p.getName());
            ibutton[i].setBounds(95, height, 200, 50);
            background.add(ibutton[i]);
            ibutton[i].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    PokemonStatus pi = new PokemonStatus(trainer, p, 0);
                }
            });
            i++;
            height+=70;
        }
        System.out.println("--------------------------------");

        setSize(430, 820);
        setVisible(true);
    }
}
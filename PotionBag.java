
public class PotionBag {
    private String name;
    private int HPup;
    private int EA;
    private String URL_Potion;
    public PotionBag(String name, int HPup, int EA, String URL_Potion){
        this.name = name;
        this.HPup = HPup;
        this.EA = EA;
        this.URL_Potion = URL_Potion;
    }
    public void setEA(int EA){
        this.EA = EA;
    }
    public String getName(){
        return name;
    }
    public int getHPup(){
        return HPup;
    }
    public int getEA(){
        return EA;
    }
    public String getURL_Potion(){
        return URL_Potion;
    }
}
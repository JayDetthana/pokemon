
public abstract class Pokemon {
    private String name;
    private int HP;
    private int MaxHP;
    private String RACE;
    private int ATK;
    private int DEF;
    private String URL_pic;
    private String URL_logo;
    public Pokemon(String name, String RACE, int MaxHP, int ATK, int DEF, String URL_pic, String URL_logo){
        this.name = name;
        this.RACE = RACE;
        this.HP = MaxHP;
        this.MaxHP = MaxHP;
        this.ATK = ATK;
        this.DEF = DEF;
        this.URL_pic = URL_pic;
        this.URL_logo = URL_logo;
    }
    public void SetHP(int HP){
        this.HP = HP;   
    }
    public String getName(){
        return name;
    }
    public int getHP(){
        return HP;
    }
    public int getMaxHP(){
        return MaxHP;
    }
    public String getRACE(){
        return RACE;
    }
    public int getATK(){
        return ATK;
    }
    public int getDEF(){
        return DEF;
    }
    public String getURL_pic(){
        return URL_pic;
    }
    public String getURL_logo(){
        return URL_logo;
    }
    public boolean Damage(int value){
        if(HP == 0){
            System.out.println("HP : "+ HP);
            return false;
        }
        int current_HP = HP - value;
        if(current_HP > 0){
            this.HP = current_HP;
            System.out.println("HP : " + current_HP);
        }
        else if(current_HP <= 0){
            System.out.println("HP : " + HP);
            current_HP = 0;
            this.HP = current_HP;
        }
        return true;
    }
    public boolean Health(Trainer trainer){
        int value = trainer.getPotionbag().getHPup();
        int current_HP = HP + value;
        trainer.getPotionbag().setEA(trainer.getPotionbag().getEA() - 1);
        if(current_HP < getMaxHP()){
            this.HP = current_HP;
            System.out.println("HP : " + current_HP);
        }
        else if(current_HP >= getMaxHP()){
            current_HP = getMaxHP();
            System.out.println("HP : " + HP);
            this.HP = current_HP;
        }
        return true;
    }

    public abstract void attack(Pokemon enemy);
    
    public String toString(){
        return name;
    }

}
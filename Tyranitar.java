
public class Tyranitar extends Pokemon{
    public Tyranitar(String name){
        super(name, "Tyranitar", (int)(Math.random() * 150), 32, 8, "pic\\tyranitar01.gif", "pic\\tyranitar02.gif");
    }
    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + getName() + " attack " + enemy.getName());
        int value = 32 - (int)(enemy.getDEF());
        if(value <= 0){
            enemy.Damage(1);
        }else{
            enemy.Damage(value);
        }// enemy.Damage(15);
    }
}
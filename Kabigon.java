public class Kabigon extends Pokemon{
    public Kabigon(String name){
        super(name, "Kabigon", (int)(Math.random() * 150), 35, 7, "pic\\kabigon02.gif", "pic\\kabigon01.gif");
    }
    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + getName() + " attack " + enemy.getName());
        int value = 35 - (int)(enemy.getDEF());
        if(value <= 0){
            enemy.Damage(1);
        }else{
            enemy.Damage(value);
        }// enemy.Damage(15);
    }
}

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.jar.JarEntry;

public class ShopStore extends JFrame{
    PotionBag myPotionBag;
    private Trainer trainer;
    public ShopStore(Trainer trainer){
        super("Store");
        this.myPotionBag = trainer.getPotionbag();
        this.trainer = trainer;

        Container c = getContentPane();

        JLabel name = new JLabel(myPotionBag.getName());
        name.setFont(new Font("Courier New", Font.ITALIC, 20));
        JLabel price = new JLabel("1x : $50");
        price.setFont(new Font("Courier New", Font.ITALIC, 20));
        JLabel total_potion = new JLabel("Total : " + myPotionBag.getEA());
        total_potion.setFont(new Font("Courier New", Font.ITALIC, 20));
        JLabel your_money = new JLabel("Your money : $" + trainer.getMoney());
        your_money.setFont(new Font("Courier New", Font.ITALIC, 20));

        ImageIcon img = new ImageIcon("pic\\scroll01.png");
        JLabel background = new JLabel(img);

        ImageIcon icon_potion = new ImageIcon(myPotionBag.getURL_Potion());
        JLabel iconpotion = new JLabel(icon_potion);

        JButton buy = new JButton();
        buy.setIcon(new ImageIcon("pic\\buy_button.png"));

        name.setBounds(100, 220, 1000, 100);
        price.setBounds(100, 260, 1000, 100);
        total_potion.setBounds(100, 300, 1000, 100);
        your_money.setBounds(100, 590, 1000, 100);
        iconpotion.setBounds(120, 100, 150, 150);
        buy.setBounds(90, 420, 200, 50);
        System.out.println("Money : " + trainer.getMoney());
        
        c.add(background);
        background.add(iconpotion);
        background.add(name);
        background.add(price);
        background.add(total_potion);
        background.add(your_money);
        background.add(buy);
        pack();
        
        int current_money = trainer.getMoney();
        if(current_money < 50){
            background.remove(buy);
        }

        buy.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                // if(trainer.getMoney() < 50){
                    setVisible(false);
                    myPotionBag.setEA(myPotionBag.getEA() + 1);
                    trainer.setMoney(trainer.getMoney() - 50);
                    ShopStore ps = new ShopStore(trainer);
                // }else{
                //     trainer.setMoney(trainer.getMoney() - 50);
                //     myPotionBag.setEA(1);
                //     total_potion.setText("Total : " + myPotionBag.getEA());
                // }
            }
        });

        setSize(430, 820);
        setVisible(true);
    }
}
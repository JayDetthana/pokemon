
import java.util.*;

public class PokemonRandomrizer {
    public static ArrayList<Pokemon> getPokemons(int num){
        ArrayList<Pokemon> Pokemons = new ArrayList<Pokemon>();

        int pokemonNumber = (int)(Math.random() * num) + 1;
        
        for(int i = 0 ; i < pokemonNumber; i++){
            int type = (int)(Math.random() * 5);
            if(type == 0){ 
                Pokemon Pikachu = new Pikachu("Wild Pikachu");
                Pokemons.add(Pikachu);
            }else if(type == 1){
                Pokemon Tyranitar = new Tyranitar("Wild Tyranitar");
                Pokemons.add(Tyranitar);
            }else if(type == 2){
                Pokemon Raichu = new Raichu("Wild Raichu");
                Pokemons.add(Raichu);
            }else if(type == 3){
                Pokemon Balbasaur = new Balbasaur("Wild Balbasaur");
                Pokemons.add(Balbasaur);
            }else if(type == 4){
                Pokemon Kabigon = new Balbasaur("Wild Kabigon");
                Pokemons.add(Kabigon);
            }
        }
        return Pokemons;
    }
}